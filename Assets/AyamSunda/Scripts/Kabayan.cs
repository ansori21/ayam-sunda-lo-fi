﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kabayan : MonoBehaviour
{
    [SerializeField] Image kabayan;
    [SerializeField] Image atta;

    private AudioFX audioFX;
    private Button btnKeplak;
    private Animator animator;

    private float animSpeed;

    void Start()
    {
        audioFX = FindObjectOfType<AudioFX>();
        animator = GetComponent<Animator>();

        btnKeplak = GetComponent<Button>();
        btnKeplak.onClick.AddListener(OnDikeplak);

        animSpeed = animator.speed;

        StartCoroutine(TuruRandom());
    }

    void OnDikeplak()
    {
        StartCoroutine(DoKaget());
    }

    IEnumerator TuruRandom()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(7f, 10f));

            var newRotation = kabayan.transform.position;
            newRotation.z = 270;
            kabayan.transform.eulerAngles = newRotation;

            animator.speed = 0;
        }
    }

    IEnumerator DoKaget()
    {
        animator.speed = animSpeed;

        // Wake up
        var newRotation = kabayan.transform.position;
        newRotation.z = 0;
        kabayan.transform.eulerAngles = newRotation;

        // Play Effect
        audioFX.PlaySlap();

        yield return new WaitForSeconds(1f);

        atta.enabled = true;
        audioFX.PlayAshiap();

        yield return new WaitForSeconds(1f);

        atta.enabled = false;
    }
}
