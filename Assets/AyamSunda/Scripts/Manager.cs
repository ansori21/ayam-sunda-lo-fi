﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    [SerializeField] TMP_Text txtScore;
    [SerializeField] Image loadingBar;
    [SerializeField] CornSpawner spawner;
    [SerializeField] Chicken chicken;

    private string typedString = "";
    private int score = 0;
    private float progress = 100f;

    private int funkIndex = 0;
    private int funkWave = 2;

    private Coroutine loadingCoroutine = null;

    void Start()
    {
        chicken.SetData(this, spawner.SpotCount());

        loadingCoroutine = StartCoroutine(RunLoadingBar());
        GenerateCorn();
    }

    void Update()
    {
        if (!Input.anyKeyDown)
            return;

        typedString = GetWordTyped(typedString);
        Checking();
    }

    void Checking()
    {
        var i = 0;
        var resetAll = true;

        while (i < spawner.cornList.Count)
        {
            var item = spawner.cornList[i];

            item.isActivated = item.IsMatched(typedString);

            if (item.isActivated)
            {
                item.SetTypedWord(typedString);
                resetAll = false;

                if (item.IsSameLength(typedString))
                {
                    resetAll = true;

                    item.SetComplete();
                    chicken.AddCornSequence(item);
                    spawner.RemoveFromList(item);

                    continue;
                }
            }
            else
                item.ResetWord();

            i++;
        }

        if (resetAll)
            typedString = "";
    }

    string GetWordTyped(string typed)
    {
        foreach (char letter in Input.inputString)
        {
            typed += letter;
        }

        return typed;
    }

    public void GenerateCorn()
    {
        spawner.SpawnAll(funkIndex >= funkWave);
        funkIndex++;
    }

    public void AddScore()
    {
        score++;
        txtScore.text = $"Score: {score}";
    }

    public void AddProgress()
    {
        progress += progress + 30 <= 100 ? 30 : 0;

        if (loadingCoroutine != null)
        {
            StopCoroutine(loadingCoroutine);
            loadingCoroutine = null;
        }

        loadingCoroutine = StartCoroutine(RunLoadingBar());
    }

    public IEnumerator GenerateCornWithDelay()
    {
        yield return new WaitForSeconds(1.5f);

        GenerateCorn();
    }

    IEnumerator RunLoadingBar()
    {
        while (progress > 0)
        {
            yield return new WaitForSeconds(0.1f);

            progress -= 1;
            loadingBar.fillAmount = progress / 100f;
        }
    }
}
