﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Corn : MonoBehaviour
{
    [SerializeField] TMP_Text cornText;

    public bool isActivated { get; set; }
    public bool isComplete { get; set; }

    private string word;
    private string colorCode = "#2e1263";


    public void SetCornWord(string word)
    {
        this.word = word;

        UpdateText(word);
    }

    public bool IsMatched(string typed)
    {
        bool matched = true;

        for (int i = 0; i < typed.Length; i++)
        {
            if (i + 1 > word.Length)
                break;

            if (typed[i] != word[i])
            {
                matched = false;
                break;
            }
        }

        return matched;
    }

    public bool IsSameLength(string typed)
    {
        return typed.Length == word.Length;
    }

    public void SetTypedWord(string typed)
    {
        if (typed.Length > word.Length || isComplete)
            return;

        string remain = word.Substring(typed.Length, word.Length - typed.Length);
        string result = $"<color={colorCode}>{typed}</color>{remain}";

        UpdateText(result);
    }

    public void SetComplete()
    {
        isComplete = true;
    }

    private void UpdateText(string text)
    {
        cornText.text = text;
    }

    public void ResetWord()
    {
        if (!isComplete)
            UpdateText(word);
    }
}
