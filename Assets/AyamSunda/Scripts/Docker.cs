﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Docker : MonoBehaviour
{
    [SerializeField] Image dockerApp;

    private Button btnTrigger;

    void Start()
    {
        btnTrigger = GetComponent<Button>();
        btnTrigger.onClick.AddListener(OnClicked);
    }

    void OnClicked()
    {
        dockerApp.enabled = !dockerApp.enabled;
    }
}
