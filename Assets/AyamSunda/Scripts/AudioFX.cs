﻿using UnityEngine;
using UnityEngine.UI;

public class AudioFX : MonoBehaviour
{
    private string audioPath = "Sounds/";

    private const string button = "sound_click-btn";
    private const string hit = "mbek_new";
    private const string slap = "Slap";
    private const string ashiap = "Ashiap";

    private AudioClip button_clip;
    private AudioClip hit_clip;
    private AudioClip slap_clip;
    private AudioClip ashiap_clip;
    
    private AudioSource soundFX;
    public static AudioFX instance = null;
    
    void Awake() 
    {
        if (instance != null && instance != this) {
            Destroy(gameObject);
            return;
        } 
        else 
            instance = this;

        DontDestroyOnLoad(gameObject);
    }

    void Start() 
    {
        soundFX = GetComponent<AudioSource>();
        LoadSound();
    }

    void LoadSound() 
    {
        button_clip = Resources.Load<AudioClip>(audioPath + button);
        hit_clip = Resources.Load<AudioClip>(audioPath + hit);
        slap_clip = Resources.Load<AudioClip>(audioPath + slap);
        ashiap_clip = Resources.Load<AudioClip>(audioPath + ashiap);
    }

    void PlaySFX() 
    {
        soundFX.Stop();
        soundFX.Play();
    }

    public void PlayButton()
    {
        soundFX.clip = button_clip;
        PlaySFX();
    }

    public void PlayHit()
    {
        soundFX.clip = hit_clip;
        PlaySFX();
    }

    public void PlaySlap()
    {
        soundFX.clip = slap_clip;
        PlaySFX();
    }

    public void PlayAshiap()
    {
        soundFX.clip = ashiap_clip;
        PlaySFX();
    }
}
