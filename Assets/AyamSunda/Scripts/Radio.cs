﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Radio : MonoBehaviour
{
    [SerializeField] private Image sore;
    [SerializeField] private Image malam;

    private AudioBGM audioBGM;
    private Button button;

    private int bgmTotal = 3;
    private int bgmIndex = 0;

    void Start()
    {
        audioBGM = FindObjectOfType<AudioBGM>();

        button = GetComponent<Button>();
        button.onClick.AddListener(() => {
            bgmIndex = bgmIndex < bgmTotal - 1 ? bgmIndex + 1 : 0;
            ChangeBGM();
        });
    }

    private void ChangeBGM()
    {
        sore.enabled = false;
        malam.enabled = false;

        switch (bgmIndex)
        {
            case 0:
                audioBGM.PlayBgmMenu();
                break;

            case 1:
                audioBGM.PlayBgmGusdur();
                sore.enabled = true;
                break;

            case 2:
                audioBGM.PlayBgmTombo();
                malam.enabled = true;
                break;
        }
    }
}
