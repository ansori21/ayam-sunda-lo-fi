﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : MonoBehaviour
{
    public float speed = 0.7f;

    private Manager manager;
    private int spotCount;

    private List<Corn> cornSequences = new List<Corn>();
    private Animator animator;
    private AudioFX audioFX;
    private Corn touchedCorn;
    private bool isMirror;

    private Coroutine spawnCoroutine = null;
    private int eatCount = 0;

    public void Start()
    {
        animator = GetComponent<Animator>();
        audioFX = FindObjectOfType<AudioFX>();
    }

    public void SetData(Manager manager, int spotCount)
    {
        this.manager = manager;
        this.spotCount = spotCount;
    }

    public void AddCornSequence(Corn corn)
    {
        cornSequences.Add(corn);
        StartCoroutine(DoEat());
    }

    public void RemoveCornSequence(Corn corn)
    {
        cornSequences.Remove(corn);
    }

    IEnumerator DoEat()
    {
        if (cornSequences.Count > 0)
        {
            var target = cornSequences[0].gameObject.transform;

            // Set mirror
            var newScale = transform.localScale;

            if (isMirror && target.position.x <= transform.position.x || 
                !isMirror && target.position.x > transform.position.x)
            {
                isMirror = !isMirror;
                newScale.x *= -1;
            }

            transform.localScale = newScale;

            // Walk
            while (cornSequences.Count > 0)
            {
                float step = speed * Time.deltaTime; // calculate distance to move

                transform.position = Vector3.MoveTowards(transform.position, target.position, step);

                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Corn"))
        {
            var item = collision.gameObject.GetComponent<Corn>();

            touchedCorn = item;
            RemoveCornSequence(item);

            animator.SetBool("eating", true);
            audioFX.PlayHit();
        }
    }

    public void StopEat()
    {
        animator.SetBool("eating", false);
        Destroy(touchedCorn.gameObject);

        if (++eatCount >= spotCount)
        {
            Debug.Log(eatCount);

            if (spawnCoroutine != null)
            {
                StopCoroutine(spawnCoroutine);
                spawnCoroutine = null;
            }

            spawnCoroutine = StartCoroutine(manager.GenerateCornWithDelay());
            eatCount = 0;
        }

        manager.AddProgress();
        manager.AddScore();
    }
}
