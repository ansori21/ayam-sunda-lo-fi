﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBGM : MonoBehaviour
{
    private string audioPath = "Sounds/";

    private const string bgmMenu = "Sabilulungan1";
    private const string bgmGusdur = "Gusdur";
    private const string bgmTombo = "TomboAti";

    private AudioClip bgmMenu_clip;
    private AudioClip bgmGusdur_clip;
    private AudioClip bgmTombo_clip;

    private AudioSource soundFX;
    public static AudioBGM instance = null;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
            instance = this;

        DontDestroyOnLoad(gameObject);
        LoadSound();
    }

    void Start()
    {
        soundFX = GetComponent<AudioSource>();
    }

    void LoadSound()
    {
        bgmMenu_clip = Resources.Load<AudioClip>(audioPath + bgmMenu);
        bgmGusdur_clip = Resources.Load<AudioClip>(audioPath + bgmGusdur);
        bgmTombo_clip = Resources.Load<AudioClip>(audioPath + bgmTombo);
    }

    void PlaySFX()
    {
        soundFX.Stop();
        soundFX.Play();
    }

    public void PlayBgmMenu()
    {
        soundFX.volume = 0.663f;
        soundFX.clip = bgmMenu_clip;
        PlaySFX();
    }

    public void PlayBgmGusdur()
    {
        soundFX.volume = 0.5f;
        soundFX.clip = bgmGusdur_clip;
        PlaySFX();
    }

    public void PlayBgmTombo()
    {
        soundFX.volume = 0.5f;
        soundFX.clip = bgmTombo_clip;
        PlaySFX();
    }
}
