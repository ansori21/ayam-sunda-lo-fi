﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WordGenerator
{
    private static List<char> chars = new List<char>("abcdefghijklmnopqrstuvwxyz0123456789");
    private static List<string> hardList = new List<string>(new string[] {
        "https://meet.google.com/zaf-ngfp-rzk",
        "Permisi mas mbak dan teman teman, ingin mereminder untuk jam 20.00 nanti bisa langsung masuk ke grup discord ruang 'Meja masing masing' ya untuk melanjutkan agenda game",
        "Assalamualaikum pak Safrodin, saya Rahmat Ansori dari kelas 1 Game Technology dengan NRP 4210171010 ingin mengumpulkan Tugas 4 Pengantar Psikologi. Terima kasih",
        "#include <stdio.h> using namespace std; int main() { print('Hello World'); }",
    });

    //private static List<string> hardList = new List<string>(new string[] {
    //    "hello",
    //    "world",
    //    "wow",
    //    "nais",
    //    "good",
    //});

    public static string RandomEasy()
    {
        int randomIndex = Random.Range(0, chars.Count);
        char randomChar = chars[randomIndex];

        chars.RemoveAt(randomIndex);

        return randomChar.ToString();
    }

    public static string RandomHard()
    {
        int randomIndex = Random.Range(0, hardList.Count);
        string randomWord = hardList[randomIndex];

        hardList.RemoveAt(randomIndex);

        return randomWord;
    }
}
