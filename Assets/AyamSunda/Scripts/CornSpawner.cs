﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] spots;
    [SerializeField] private Corn cornPrefab;

    public List<Corn> cornList = new List<Corn>();

    public int SpotCount()
    {
        return spots.Length;
    }

    public void SpawnAll(bool funkMode = false)
    {
        int funkIndex = Random.Range(0, spots.Length);

        for (var i = 0; i < spots.Length; i++)
        {
            var item = spots[i];
            var cornObj = Instantiate(cornPrefab, item.transform);

            var corn = cornObj.GetComponent<Corn>();

            if (funkMode && funkIndex == i)
            {
                corn.SetCornWord(WordGenerator.RandomHard());
                funkMode = false;
            }
            else
                corn.SetCornWord(WordGenerator.RandomEasy());

            cornList.Add(corn);
        }
    }

    public void RemoveFromList(Corn item)
    {
        cornList.Remove(item);
    }
}
